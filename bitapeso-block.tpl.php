<div id="tabs-container">
    <ul class="tabs-menu">
        <li class="current"><a href="#tab-1">Cambio</a></li>
        <li><a href="#tab-2">Calcular</a></li>
    </ul>
    <div class="tab">
        <div id="tab-1" class="tab-content">
            <p>Un dolar = $<span id="usdtopeso"><?php echo $undolar; ?></span> <span class="divisa">MXN</span></p>
            <p>1 Bitcoin = $<span id="bittopeso"><?php echo $unbitcoinmxn; ?></span> <span class="divisa">MXN</span></p>
            <p>1 Bitcoin = $<span id="bittousd"><?php echo $unbitcoinusd; ?></span> <span class="divisa">USD</span></p>
            <p class="leyender"><a href="http://bitapeso.com/" title="Bit a Peso" alt="Bit a Peso"target="_blank" target="_blank">BitaPeso</a> by <a href="https://ninja-code.de/" title="Ninja Code" alt="Ninja Code" target="_blank">Ninja Code</a></p>
        </div>
        <div id="tab-2" class="tab-content">
            <p><label>Ingrese los pesos a convertir</label></p>
            <p>$<input type="text" name="peso" id="peso" value="<?php echo $cantidad; ?>" maxlength="10" size="10"><span class="divisa">MXN</span></p>
            <div id="resultadosBit"><p id="resultadosBitp"></p></div>
            <p class="leyender"><a href="http://bitapeso.com/" title="Bit a Peso" alt="Bit a Peso" target="_blank">BitaPeso</a> by <a href="https://ninja-code.de/" title="Ninja Code" alt="Ninja Code" target="_blank">Ninja Code</a></p>
        </div>
    </div>
</div>