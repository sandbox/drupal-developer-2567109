DISCLAIMER 

Permite una infraestructura de pagos descentralizada, sin grandes oligopolistas que carguen tasas por realizar operaciones (como cobrar con tarjeta en los comercios). Ya existen cientos de aplicaciones y webs que permiten la libre transmisión de Bitcoins en comercios, con una comisión del 1%.